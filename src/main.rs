use serde_json::Value;
use std::fs;
use drs_primitives::*;
use std::collections::BinaryHeap;
use ordered_float::NotNan;
use bitflags::*;

mod tests;

const TOLERANCE: f64 = 1e-6;

//pub trait TrainServiceProvider

bitflags! {
    //flags for the edges in the graph
    struct EdgeFlags: u32 {
        const NOUGHT =      0b00000000;
        const WALKABLE =    0b00000001;
        const BIKE =        0b00000010;
        const BUS =         0b00000100;
    }
}

//represents a node in a graph
pub struct Node {
    points: Vec<(usize,usize)>, //(road index, point index in road)
    pos: Coord, //spacial position of the node
    ind: usize, //index of the node in the graph
    adjlist: Vec<(usize,f64,EdgeFlags)> //adjacency list (connected node, distance, flags)
}

//returns shortest path in graph g frome node with index s to node with index t using A*
fn shortest_path(g: &Vec<Node>, s: usize, t: usize)->Vec<usize>{
    //res vector will store the answer
    let mut res: Vec<usize> = Vec::new();
    //dist vector will store in the ith position, the shortest distance from s found for the ith node
    let mut dist: Vec<NotNan<f64> > = Vec::new();
    //father will store were you came from for the shortest path
    let mut father: Vec<usize> = Vec::new();
    //binary heap used for A* algorithm (without decrease key), contains (distance from s+spacial distance from t, index of current node)
    let mut heap: BinaryHeap<std::cmp::Reverse<(NotNan<f64>, usize)> > = BinaryHeap::new();
    //initialize the minimum distance to 100000 km
    dist.resize(g.len(),NotNan::from(100000.0));
    //initialize the fathers to the first impossible index
    father.resize(g.len(),g.len());
    //set distance of the source vertex into the graph and push it onto the heap
    dist[s]=NotNan::from(0.0);
    heap.push(std::cmp::Reverse( (NotNan::from(g[s].pos.distance(&g[t].pos)),s) ));
    while !heap.is_empty() { //if the while ends for this statement, no path was found
        let p = heap.pop().unwrap().0; //safe because of while statement
        if dist[s]+NotNan::from(g[p.1].pos.distance(&g[t].pos))>p.0 { //if a better path to p.1 was found, ignore this one
            continue;
        }
        if p.1 == t { //got to the destination vertex
            break;
        }
        for i in &g[p.1].adjlist { //iterate through the adjlist
            let nd = NotNan::from(i.1)+dist[p.1]; //distance if you were to go from p to i.0
            let np = nd+NotNan::from(g[i.0].pos.distance(&g[t].pos)); //priority if you were to go from p to i.0
            if dist[i.0]>nd { //if a better path (or the first one) was found for i.0
                //set his father to me, his distance to the newfound one and push it onto the heap
                father[i.0]=p.1;
                dist[i.0]=nd;
                heap.push(std::cmp::Reverse( (np,i.0) ));
            }
        }
    }
    if father[t] == g.len() { //no path was found from s to t
        return vec![];
    }
    println!("got there with dist: {}", dist[t]); //deebug
    let mut p = t;
    while p!=s { //iterate through the vertexes you went through in reverse order
        res.push(p);
        p=father[p];
    }
    res.push(s);
    res.reverse(); //reverse it so they are in correct order
    return res; //return the shortest path from s to t
}

fn main() {
    let osm_raw_geojson = fs::read("res/puliti1.geojson").unwrap();
    let osm_data: Value = serde_json::from_slice(&osm_raw_geojson).unwrap();
    let roads = osm_data["features"].as_array().unwrap();
    let mut osm_parsed: Vec<Road> = Vec::new();
    let mut points: Vec<(usize,usize,Option<usize>)> = Vec::new(); //(strada index, point index in strada, node index)
    for road in roads{
        osm_parsed.push(Road{
            segments: vec![],
            points: vec![],
            name: match road["properties"]["name"].as_str(){
                Some(x) => Some(String::from(x)),
                None => None
            },
            forbidden_to_bikes: false,
            forbidden_to_pedestrians: false
        });
        let raw_coords = road["geometry"]["coordinates"].as_array().unwrap();
        let layer = match road["properties"]["layer"].as_str() {
            Some(x) => x.parse::<i8>().unwrap_or(0),
            None => 0
        };
        for i in 0..raw_coords.len(){
            osm_parsed.last_mut().unwrap().points.push(Coord{
                lon: raw_coords[i][0].as_f64().unwrap(),
                lat: raw_coords[i][1].as_f64().unwrap()
            });
            points.push((osm_parsed.len()-1,osm_parsed.last().unwrap().points.len()-1,None));
        }
        for i in 1..raw_coords.len(){
            osm_parsed.last_mut().expect("boh").segments.push(Segment{a: Coord{
                lon: raw_coords[i][0].as_f64().unwrap(),
                lat: raw_coords[i][1].as_f64().unwrap()
            },
            b: Coord{
                lon: raw_coords[i][0].as_f64().unwrap(),
                lat: raw_coords[i][1].as_f64().unwrap()
            }, layer: Some(layer)});
        }
    }

    //Graph
    let mut nodes: Vec<Node> = Vec::new();
    //sort points to group all the same points together to later create the nodes for the graph in O(N)
    points.sort_by(|a,b| osm_parsed[a.0].points[a.1].partial_cmp(&osm_parsed[b.0].points[b.1]).unwrap());
    //iterate through the points and create a Node for all the groups of equal points with cardinality greater than 1
    let mut i = 0;
    while i<points.len() { //iterate through the points
        //push the node for this point (will be removed later if unnecessary)
        nodes.push(Node{
            points: vec![(points[i].0,points[i].1)],
            pos: osm_parsed[points[i].0].points[points[i].1],
            ind: nodes.len(),adjlist:vec![]
        });
        points[i].2=Some(nodes.len()-1);
        let mut j = i+1;
        while j<points.len()&&osm_parsed[points[i].0].points[points[i].1].distance(&osm_parsed[points[j].0].points[points[j].1])<TOLERANCE { //same point in space, add it to the vertex
            nodes.last_mut().unwrap().points.push((points[j].0,points[j].1));
            points[j].2=Some(nodes.len()-1);
            nodes.last_mut().unwrap().points.push((points[j].0,points[j].1));
            points[j].2=Some(nodes.len()-1);
            j+=1;
        }
        if j==i+1 { //no other point was added, thus point is not an intersection of any kind and it should thus have no node associated
            nodes.pop();
            points[i].2=None;
        }
        i=j;
    }
    //sort points lexicographically by street, position in street
    points.sort();
    //last point's distance from the start of the road
    let mut last_dist: f64 = 0.0;
    //last point's index in the road
    let mut last_index: usize=0;
    let mut flags: EdgeFlags = EdgeFlags::NOUGHT;
    for i in 0..points.len() {
        if points[i].2==None { //Point had no equal points, so it did not become a node
            continue;
        } else if i==0 || points[last_index].0!=points[i].0 { //First point of some road
            //initialize the flags for this road
            flags = if osm_parsed[points[i].0].forbidden_to_pedestrians { EdgeFlags::NOUGHT } else { EdgeFlags::WALKABLE };
            //calculate distance from the start of the road to this point
            last_dist=0.0;
            for j in 0..points[i].1 {
                last_dist+=osm_parsed[points[i].0].points[j].distance(&osm_parsed[points[i].0].points[j+1]);
            }
            last_index=i;
        } else { //A point that is not the first of a road
            //calculate distance from the start of the road to this point
            let mut dist: f64 = last_dist;
            for j in points[last_index].1..points[i].1 {
                dist += osm_parsed[points[i].0].points[j].distance(&osm_parsed[points[i].0].points[j+1]);
            }
            if points[i].2.unwrap() != points[last_index].2.unwrap() { //check that they do not belong to the same node (should never be true)
                //push the forward and backward edge
                nodes[points[last_index].2.unwrap()].adjlist.push((points[i].2.unwrap(),dist-last_dist,flags));
                nodes[points[i].2.unwrap()].adjlist.push((points[last_index].2.unwrap(),dist-last_dist,flags));
            }
            last_index=i;
        }
    }

    println!("{} nodes",nodes.len());

    let sp = shortest_path(&nodes, 234, 423);
    
    println!("{:?}",sp);

    //print!("{{\"type\": \"FeatureCollection\", \"features\": [");

    /*for i in 0 .. strade_parsed.len() {
        println!("=== {:?} ({:?}) ===", strade_parsed[i].name, strade_parsed[i].center());
        for intersection in &intersections[i] {
            println!("{{\"geometry\":{{\"type\": \"Point\", \"coordinates\": [{},{}]}}, \"type\": \"Feature\", \"properties\":{{\"name\":\"{} inc. {}\"}}}},", intersection.1.lon, intersection.1.lat, strade_parsed[i].name.as_ref().unwrap_or(&String::from("-")), strade_parsed[intersection.0].name.as_ref().unwrap_or(&String::from("-")));
        }
    }*/
    /*for i in nodes {
        println!("{{\"geometry\":{{\"type\": \"Point\", \"coordinates\": [{},{}]}}, \"type\": \"Feature\", \"properties\":{{\"name\":\"{} inc. {}\"}}}},", i.pos.lon, i.pos.lat, &i.ind.to_string(), &String::from("-"));
    }*/
    /*for i in sp {
        println!("{{\"geometry\":{{\"type\": \"Point\", \"coordinates\": [{},{}]}}, \"type\": \"Feature\", \"properties\":{{\"name\":\"{} inc. {}\"}}}},", nodes[i].pos.lon, nodes[i].pos.lat, &nodes[i].ind.to_string(), &String::from("-"));
    }*/

    //println!("]}}");
    
    /*let gtfs = Gtfs::new("res/gtfs").expect("impossible to read gtfs");
    println!("Letto file");
    gtfs.print_stats();
    for (wtf,stop) in gtfs.stops.iter() {
        println!("{} -> {:?}", wtf, stop);
    }*/
}
